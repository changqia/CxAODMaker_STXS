#!/usr/bin/env bash

# forbids an interactive shell from running this executable, in other words, do not source
if [[ $- == *i* ]] ; then
    echo "ERROR: I'm a script forcing you to execute. Don't source me!" >&2
    return 1
else
    # if I am OK to execute, force that the script stops if variables are not defined
    # this catches bugs in the code when you think a variable has value, but it is empy
    set -eu
fi

# check the number of parameters, if not stop
if [ $# -ne 2 ]; then
cat <<EOF
Usage: $0 packages_file                     FORCE_CHECKOUT
Usage: $0 packages-VHbb-svn-master.txt 0
EOF
exit 1
fi

packages_list=$1
FORCE_CHECKOUT=$2

# start loop over CxAODFramework packages
while read line
do
    #
    package=$(echo "$line" | awk '{print $1}')

    # skip those that are commented out starting with #
    if [[ ${package:0:1} == "#" ]] ; then
	continue
    fi

    tag=$(echo "$line" | awk '{print $2}')
    echo "package=${package} tag=${tag}"

    #
    if [[  -d $package ]] ; then
        if [[ $FORCE_CHECKOUT == "1" ]] ; then
            rm -rf $package
        else
            echo "$package already checked out"
            continue
        fi
    fi



    #
    if [[ $package == "HFORtool" ]] ; then
	# this package does not have a tag in SVN, so we check a given revision
	# see https://its.cern.ch/jira/browse/CXAOD-341
	COMMAND1="svn co -r 162 svn+ssh://svn.cern.ch/reps/atlas-leite/HFORtool/trunk HFORtool"
	echo "COMAND1=${COMMAND1} (checkout out a revision as this package does noth have a tag)"
	${COMMAND1}
    else
	# regular package that has a tag in SVN, so
        # checkout with rc checkout
        # note $tag, not $package, as we need the full path, which we store in $tag
	FILE_RC="rc_temp.txt"
	touch ${FILE_RC}
        # create the script that will be run
        (
	    echo "$tag"
	) > "${FILE_RC}"
	COMMAND1="rc checkout ${FILE_RC}"
	echo "COMAND1=${COMMAND1} (checkout a text file containing $tag)"
	${COMMAND1}
	rm -f ${FILE_RC}
    fi # end if package has a given name

    # if folder does not exist, continue
    if [ ! -d "$package" ]; then
	echo "WARNING!!! package ${package} does not have a folder!"
	continue
    fi
    # done all for current package
done < $packages_list
# done loop over all the packages

echo "Done the checkout of svn packages using rc checkout!"
