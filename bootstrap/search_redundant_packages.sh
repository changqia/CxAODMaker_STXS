#!/bin/bash

echo "Calling 'rc find_packages' to search for redundant packages..."
echo "Expected packages listed in: $@"

currDir=$(pwd)
packagesExpected=$(cat $@ | sort | uniq | awk '{print $1;}')
packagesLocal=$(rc find_packages | grep $currDir/ | sort | uniq | sed s@$currDir/@@)

# FrameworkSub is expected, but not in the list:
packagesExpected="$packagesExpected FrameworkSub"

#echo "Expected packages:" $packagesExpected
#echo "Local packages   :" $packagesLocal

for pkg in $packagesLocal; do
  pkgTest=$(echo $packagesExpected | grep -w $pkg)
  if [ -z "$pkgTest" ]; then
    echo "WARNING: Unexpected package! Please delete if not needed: $pkg"
  fi
done

