#!/usr/bin/env bash
# bootstrap script for VHbb
# Adrian Buzatu, adrian.buzatu@cern.ch, 12 Oct 2016
# with inspiration from Dan Guest for checkout_packages
# and from getTrunk.sh from previous personal bootstrap script 

# 1. setup ATLAS and load a newer version of git
setupATLAS
lsetup git

BOOTSTRAPDIR=CxAODMaker_STXS/bootstrap/

# 2. check out git packages
echo "STEP 2: checkout git packages"
if ! ${BOOTSTRAPDIR}/checkout_packages_git.sh ${BOOTSTRAPDIR}/packages_STXS_git_master.txt 0 ; then
    echo "problem checking out git packages" >&2
    return
fi

# to check svn packages, we need to use rc checkout
# at least for now, before they are also migrated to Git
# so we need to setup RootCore already

# from here the same order as in SVN approach
# 3. setup RootCore and AnalysisBase with version taken from the release-trunk
echo "STEP 3: setup RootCore with AnalysisBase"
release=2.4.28
rcSetup -u
rcSetup Base,$release

# 4. add the rest of the packages that are from CxAOD packages
# currently the file contains also the CxAOD packages
# but they will be ignored as already checked out
#packages = VHbb-packages-svn.txt
#rc checkout $packages
echo "STEP 4: check out svn packages with rc checkout" 
if ! ${BOOTSTRAPDIR}/checkout_packages_svn.sh ${BOOTSTRAPDIR}/packages_STXS_svn_master.txt 0 ; then
    echo "problem checking out svn packages with rc checkout" >&2
    return
fi

echo "STEP 5: search for redundant packages" 
${BOOTSTRAPDIR}/search_redundant_packages.sh ${BOOTSTRAPDIR}/packages_STXS_*_master.txt

# done
return # use instead of exit when sourcing the script
