# How to setup

## Standard setup with CxAOD framework
This package relies on some features from CxAOD framework.

- CxAODTools: Normalization with SumOfWeightProvider and XSProvider
- CxAODReader: HistSvc and HistNameSvc
- CorrsAndSysts: EWK correction
- CxAODReader_VHbb: STXS split feature

Please read the instruction in [FrameworkSub](https://gitlab.cern.ch/CxAODFramework/FrameworkSub)


## Kerberos authorization
```
kinit CERN_USERNAME@CERN.CH
```

## Then setup the package and checkout the packages
After you setup the CxAODframework, you download `CxAODMaker_STXS` into `source`

And also `CxAODMaker_STXS` replied on `TruthWeightTools` to read the MC weight. (NNPDF3/PDF4LHC/etc.)

```
cd source
git clone https://:@gitlab.cern.ch:8443/changqia/TruthWeightTools.git
git clone https://:@gitlab.cern.ch:8443/changqia/CxAODMaker_STXS.git
```

## Compile
Follow the way you compile CxAOD framework. Remember `cmake ../source/` to import new packages.
```
cd build
setupATLAS
asetup --restore
cmake ../source/
make
```

# How to execute

## Edit the configuration
```
cd run
../source/CxAODMaker_STXS/data/framework-thxs.cfg
```

## Execute
```
hsg5frameworkSTXS
```

# The script to check the meta data of the xAOD
The `MCWeights` is a vector, it contains the weight needed.

This script tells you the positon of the weight in the vector and the meaning of it.

```
asetup AthAnalysisBase,2.3.34,here
/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc48-opt/2.3.34/AthAnalysisBase/2.3.34/InstallArea/share/bin/checkMetaSG.py
```

# Finer stage1 binning

![](Plots/Fine/Yields.png)
![](Plots/Fine/Purity.png)
![](Plots/Fine/Acceptance.png)

# Basic stage1 binning with NUNU split

![](Plots/Base/Yields.png)
![](Plots/Base/Purity.png)
![](Plots/Base/Acceptance.png)

