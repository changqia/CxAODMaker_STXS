#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "EventLoopAlgs/NTupleSvc.h"
#include "EventLoop/OutputStream.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "PathResolver/PathResolver.h"

#include "CxAODMaker_STXS/AnalysisBase_STXS.h"

#include <iostream>
#include <fstream>

void replaceAll( std::string &s, const std::string &search, const std::string &replace ) {
    for( size_t pos = 0; ; pos += replace.length() ) {
        pos = s.find( search, pos );
        if( pos == std::string::npos ) break;
        s.erase( pos, search.length() );
        s.insert( pos, replace );
    }
}

int main( int argc, char* argv[] ) {
  bool overrideSubmit = false;
  bool officialProduction = false;
  bool officialProductionExot = false;

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  std::string configPath = "data/CxAODMaker_STXS/framework-thxs.cfg";
  if( argc > 1 ) submitDir = argv[ 1 ];
  if( argc > 2 ) configPath = argv[2];

  // read run config
  static ConfigStore* config = ConfigStore::createStore(configPath);

  // for grid running - contains sample names from text file
  std::vector<std::string> sample_name_in;

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //std::string inputFilePath = PathResolverFindCalibDirectory("/sps/atlas/groups/Hbb/DxAOD/unskimmed_DxAOD/mc15_13TeV.345056.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_vvbb_VpT.merge.DAOD_HIGG5D1.e5706_s2726_r7772_r7676_p3231");
  std::string sample_in = "";
  config->getif<std::string>("sample_in", sample_in);
  bool grid = sample_in.find("grid") != std::string::npos;
  Info("hsg5frameworkSTXS","Run on grid = %s", grid ? "true" : "false");

  if (!grid) {
    SH::DiskListLocal list(sample_in);
    SH::ScanDir().scan(sh,list);
  }else{
    // read input file
    std::ifstream infile;
    infile.open(sample_in);
    if ( infile.fail() ) {
      Error("hsg5frameworkSTXS","Sample list file '%s' not found!", sample_in.c_str());
      return EXIT_FAILURE;
    }
    std::string line;
    while (!infile.eof()) {
      getline(infile, line);
      // remove all spaces from line
      line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
      // skip empty lines to avoid crash
      if (line.length() == 0) continue;
      // don't run over commented lines
      if (line.find("#") != std::string::npos) continue;
      // add sample name to vector
      sample_name_in.push_back(line);
    } 
    infile.close();
    // check if text file contains sample names
    if ( sample_name_in.size() == 0) {
      Error("hsg5frameworkSTXS","No samples specified in file '%s'!", sample_in.c_str());
      return EXIT_FAILURE;
    } 
    // declare the jobs
    for (unsigned int isam = 0; isam < sample_name_in.size(); ++isam) {
      SH::scanDQ2 (sh, sample_name_in[isam]);
      //os << "sample -> " << sample_name_in[isam] << std::endl;
    }
  }


  //SH::ScanDir().filePattern("DAOD_HIGG5D1.11733397._000031.pool.root.1").scan(sh,inputFilePath);


  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );

  // turn off sending xAOD summary access report (to be discussed)
  bool m_enableDataSubmission=true;
  config->getif<bool>("enableDataSubmission", m_enableDataSubmission);
  if( !m_enableDataSubmission ){
    xAOD::TFileAccessTracer::enableDataSubmission(false);
  }

  // limit number of events to maxEvents - set in config
  int eventMax = -1;
  config->getif<int>("maxEvents",eventMax);

  job.options()->setDouble (EL::Job::optMaxEvents, eventMax);

  // Add our analysis to the job:
  AnalysisBase_STXS* alg = new AnalysisBase_STXS();
  Info("hsg5frameworkSTXS", "add the config");
  alg->setConfig( config );
  // define an output and an ntuple associated to that output
  EL::OutputStream output("STXSNtuple");
  job.outputAdd( output );
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("STXSNtuple");
  job.algsAdd( ntuple );
  job.algsAdd( alg );
  alg->m_outputName = "STXSNtuple"; // give the name of the output to our algorithm

  // Run the job using the local/direct driver:
  if (!grid) {
    EL::DirectDriver driver;
    Info("hsg5frameworkSTXS", "submit the job");
    driver.submit( job, submitDir );
  } else {
    EL::PrunDriver driver;
    // determine ana_tag name
    std::string ana_tag = "ana_tag"; // default value if it cannot be determined below
    if      (sample_name_in[0].find("HIGG5D1") != std::string::npos) ana_tag = "HIGG5D1";
    else if (sample_name_in[0].find("HIGG5D2") != std::string::npos) ana_tag = "HIGG5D2";
    else if (sample_name_in[0].find("HIGG2D4") != std::string::npos) ana_tag = "HIGG2D4";
    // form needed names
    std::string vtag = "vtag"; // default in case it is not specified in steer file
    config->getif<std::string>("vtag",vtag);

    bool haveOfficialSample = false;
    bool haveGroupSample = false;
    for (std::string name : sample_name_in ) {
      if (name.find("group.") != std::string::npos ||
          name.find("user.") != std::string::npos) {
        haveGroupSample = true;
      } else {
        haveOfficialSample = true;
      }
    }

    if (haveGroupSample && haveOfficialSample) {
      Error("hsg5framework","Found official and group production samples in input files! Cannot determine output sample names.");
      return EXIT_FAILURE;
    }

    // position of tag "mc14_13TeV" or similar in input sample
    // 1 = beginning (for official production)
    // 3 = for group/user production
    int tag_offset = 1;
    if (haveGroupSample) {
      tag_offset = 3;
    }

    // output dataset name definition
    std::ostringstream ostr_sample_out_begin;
    if(officialProduction) { // use group name for officialProduction
      Info("hsg5framework","Configuring for official production");
      ostr_sample_out_begin << "group.phys-higgs.%in:name["
          << tag_offset + 0 << "]%.%in:name["
          << tag_offset + 1 << "]%.";
    }else if(officialProductionExot) { // use group name for officialProduction
      Info("hsg5framework","Configuring for official production");
      ostr_sample_out_begin << "group.phys-exotics.%in:name["
          << tag_offset + 0 << "]%.%in:name["
          << tag_offset + 1 << "]%.";
    } else { // use user name for non-official production
      ostr_sample_out_begin << "user.%nickname%.%in:name["
          << tag_offset + 0 << "]%.%in:name["
          << tag_offset + 1 << "]%.";
    }
    std::string sample_out_begin = ostr_sample_out_begin.str();
    std::ostringstream ostr_sample_out_end;
    ostr_sample_out_end << "." << ana_tag << "." << vtag;
    std::string sample_out_end = ostr_sample_out_end.str();
    // loop on all input samples
    for (unsigned int isam = 0; isam < sample_name_in.size(); ++isam) {
       // find the proper part of the string to prune
       std::string sample_out_name2 = "";
       std::string str = sample_name_in[isam];
       int count_dots = 0;
       //
       std::string sample_out_sname = ".";
       int count_underscore = 0;
       for(unsigned int i = 0 ; i < str.length(); i++) {
         if ( str[i]=='.' ) count_dots++;
         // keep generator part of the name
         else if (count_dots==(tag_offset+1) ) sample_out_name2 += str[i];
         // keep the 1st _s... of reco
         else if (count_dots>(tag_offset+3) ) {
           if ( str[i]=='_' ) count_underscore++;
           else if (count_underscore==1) sample_out_sname += str[i];
         }
       }
       // prune generators
       std::string sample_out_prune = sample_out_name2;
       replaceAll(sample_out_prune,"ParticleGenerator","PG");
       replaceAll(sample_out_prune,"Pythia","Py");
       replaceAll(sample_out_prune,"Powheg","Pw");
       replaceAll(sample_out_prune,"MadGraph","MG");
       replaceAll(sample_out_prune,"EvtGen","EG");
       replaceAll(sample_out_prune,"Sherpa","Sh");
       replaceAll(sample_out_prune,"_CT10","");
       // set the prun option -> s-tag only on MC
       if (str[0]=='d') sh.setMetaString (".*"+sample_out_name2+".*", "nc_outputSampleName", sample_out_begin+sample_out_prune+sample_out_end);
       else             sh.setMetaString (".*"+sample_out_name2+".*"+sample_out_sname+".*", "nc_outputSampleName", sample_out_begin+sample_out_prune+sample_out_sname+sample_out_end);
    }  // output job name

    //submit flags
    std::string stringOptSubmitFlags="";
    if( officialProduction || officialProductionExot) { // for official production - set the official command line option
      stringOptSubmitFlags+=" --official";
    }
    driver.options()->setString(EL::Job::optSubmitFlags, stringOptSubmitFlags.c_str());

    // other options
    double nFilesPerJob = -1.;
    config->getif<double>("nFilesPerJob",nFilesPerJob);
    if (nFilesPerJob > 0) driver.options()->setDouble("nc_nFilesPerJob", nFilesPerJob);
    //
    double nGBPerJob = -1.;
    config->getif<double>("nGBPerJob",nGBPerJob);
    if (nGBPerJob > 0) {
      if (nGBPerJob == 1000.) driver.options()->setString("nc_nGBPerJob", "MAX");
      else driver.options()->setDouble("nc_nGBPerJob", nGBPerJob);
    }
    //
    std::string excludedSite = "none";
    config->getif<std::string>("excludedSite",excludedSite);
    if (excludedSite != "none") driver.options()->setString("nc_excludedSite", excludedSite);
    //
    bool submit = false;
    if(overrideSubmit) {
      Info("hsg5framework","Grid submission disabled from command line");
    }
    config->getif<bool>("submit",submit);
    if (!submit || overrideSubmit) { // don't submit the job if the config file says not to - or if overridden from the command line
      driver.options()->setDouble("nc_noSubmit", 1);
      driver.options()->setDouble("nc_showCmd", 1);
    }
    driver.options()->setString("nc_mergeOutput", "true"); // can do for CxAOD by now
    // run
    //driver.submit(job, submitDir); // with monitoring
    driver.submitOnly(job, submitDir); // no monitoring
  }

  return 0;
}
