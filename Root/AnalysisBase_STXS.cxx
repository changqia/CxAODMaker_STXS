#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <CxAODMaker_STXS/AnalysisBase_STXS.h>
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// ASG status code check
#include <AsgTools/MessageCheck.h>

#define length(array) (sizeof(array) / sizeof(*(array)))
// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisBase_STXS)



AnalysisBase_STXS :: AnalysisBase_STXS ():
// VH EW
m_doVHNLOEWK(true),
m_corrsAndSysts(nullptr),
m_truthParts(nullptr),
// global
m_config(nullptr),
m_histSvc(nullptr),
m_histNameSvc(nullptr),
m_xSectionProvider(nullptr),
m_sumOfWeightsProvider(nullptr),
m_reader(nullptr),
m_luminosity(-999),
m_weight(1.),
m_EventNumber(0),
m_lumi(1.),
m_XS(1.),
m_SumOfWeight(1.),
m_VHEW(1.),
m_mcChannel(0),
m_HTXS_Higgs_pt(0.),
m_HTXS_Higgs_eta(0.),
m_HTXS_Higgs_phi(0.),
m_HTXS_Higgs_m(0.),
m_HTXS_V_pt(0.),
m_NominalWeight(0.),
m_Weight0(0.),
m_PDFWeight0(0.),
m_alphaS_up(0.),
m_alphaS_dn(0.),
m_PDFweights(),
m_QCDweights(),
m_HTXS_V_eta(0.),
m_HTXS_V_phi(0.),
m_HTXS_V_m(0.),
m_HTXS_prodMode(0),
m_HTXS_Stage0_Category(0),
m_HTXS_Stage1_Category_pTjet25(0),
m_HTXS_Stage1_Category_pTjet30(0),
m_HTXS_Njets_pTjet25(0),
m_HTXS_Njets_pTjet30(0),
m_isMC(false),
m_debug(false)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode AnalysisBase_STXS :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD ();
  xAOD::Init(); // call before opening first file
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  ANA_CHECK(xAOD::Init());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisBase_STXS :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  Info("histInitialize()", "Initializing histograms.");
  // histogram manager
  m_histSvc     = new HistSvc();
  m_histNameSvc = new HistNameSvc();
  m_histSvc->SetNameSvc(m_histNameSvc);
  bool fillHists = true;
  //m_config->getif<bool>("writeHistograms", fillHists);
  m_histSvc->SetFillHists(fillHists);

  // get the output file, create a new TTree and connect it to that output
  // define what braches will go in that tree
  TFile *outputFile = wk()->getOutputFile (m_outputName);
  m_tree = new TTree ("tree", "tree");
  m_tree->SetDirectory (outputFile);
  m_tree->Branch ("mcChannel", &m_mcChannel);
  m_tree->Branch ("weight", &m_weight);
  m_tree->Branch ("EventNumber", &m_EventNumber);
  m_tree->Branch ("lumi", &m_lumi);
  m_tree->Branch ("XS", &m_XS);
  m_tree->Branch ("SumOfWeight", &m_SumOfWeight);
  m_tree->Branch ("PDFweights", &m_PDFweights);
  m_tree->Branch ("QCDweights", &m_QCDweights);
  m_tree->Branch ("NominalWeight", &m_NominalWeight);
  m_tree->Branch ("Weight0", &m_Weight0);
  m_tree->Branch ("PDFWeight0", &m_PDFWeight0);
  m_tree->Branch ("alphaS_up", &m_alphaS_up);
  m_tree->Branch ("alphaS_dn", &m_alphaS_dn);
  m_tree->Branch ("VHEW", &m_VHEW);
  m_tree->Branch ("HTXS_Higgs_pt", &m_HTXS_Higgs_pt);
  m_tree->Branch ("HTXS_Higgs_eta", &m_HTXS_Higgs_eta);
  m_tree->Branch ("HTXS_Higgs_phi", &m_HTXS_Higgs_phi);
  m_tree->Branch ("HTXS_Higgs_m", &m_HTXS_Higgs_m);
  m_tree->Branch ("HTXS_V_pt", &m_HTXS_V_pt);
  m_tree->Branch ("HTXS_V_eta", &m_HTXS_V_eta);
  m_tree->Branch ("HTXS_V_phi", &m_HTXS_V_phi);
  m_tree->Branch ("HTXS_V_m", &m_HTXS_V_m);
  m_tree->Branch ("HTXS_prodMode", &m_HTXS_prodMode);
  m_tree->Branch ("HTXS_Stage0_Category", &m_HTXS_Stage0_Category);
  m_tree->Branch ("HTXS_Stage1_Category_pTjet25", &m_HTXS_Stage1_Category_pTjet25);
  m_tree->Branch ("HTXS_Stage1_Category_pTjet30", &m_HTXS_Stage1_Category_pTjet30);
  m_tree->Branch ("HTXS_Njets_pTjet25", &m_HTXS_Njets_pTjet25);
  m_tree->Branch ("HTXS_Njets_pTjet30", &m_HTXS_Njets_pTjet30);

  return EL::StatusCode::SUCCESS;

}



EL::StatusCode AnalysisBase_STXS :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisBase_STXS :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisBase_STXS :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  xAOD::TEvent* event = wk()->xaodEvent();

  // sample name
  TString sampleName = wk()->metaData()->castString("sample_name");
  Info("initialize()", "Sample name = %s", sampleName.Data());
  // as a check, let's see the number of events in our xAOD (long long int)
  Info("initialize()", "Number of events in file   = %lli", event->getEntries());

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  // count number of events
  m_eventCounter = 0;

  // option on the debug mode
  m_config->getif<bool>("debug", m_debug);

  // luminosity (for rescaling of MC to desired lumi, in fb-1)
  // default is 0.001 fb-1, which means no rescaling of the MC inputs (already scaled to 1 pb-1)
  m_luminosity = 0.001;
  m_config->getif<float>("luminosity", m_luminosity);
  Info("initializeEvent()", "Luminosity for normalisation of MC = %f fb-1", m_luminosity);

  //Xsec Provider
  //------------------
  std::string xSectionFile = gSystem->Getenv("WorkDir_DIR");
  xSectionFile      += "/data/FrameworkSub/XSections_13TeV.txt";
  // if xSectionFile is given in config file, replace all we just did
  m_config->getif<string>("xSectionFile", xSectionFile);
  m_xSectionProvider = new XSectionProvider(xSectionFile);

  if (!m_xSectionProvider) {
    Error("initializeTools()", "XSection provider not initialized!");
    return EL::StatusCode::FAILURE;
  }

  //sumOfWeights Provider
  //-----------------
  std::string sumOfWeightsFile;
  m_config->getif<string>("yieldFile", sumOfWeightsFile);
  sumOfWeightsFile = gSystem->Getenv("WorkDir_DIR")+sumOfWeightsFile;
  m_sumOfWeightsProvider = new sumOfWeightsProvider(sumOfWeightsFile);

  if (!m_sumOfWeightsProvider) {
    Error("initializeTools()", "sumOfWeightsProvider not initialized!");
    return EL::StatusCode::FAILURE;
  }

  // initialize a reader for translation
  m_reader = new AnalysisReader_VHQQ;

  // add the VH EW correcton
  // it is on defaultly
  m_doVHNLOEWK = true;
  m_config->getif<bool>("doVHNLOEWK", m_doVHNLOEWK);
  if (m_doVHNLOEWK){
    initializeCorrsAndSysts();
    if (!m_corrsAndSysts){
      Error("initializeTools()", "CorrsAndSysts not initialized!");
      return EL::StatusCode::FAILURE;
    }
  }

  // add the HiggsWeight Tool
  //m_higgsWeightTool.setMsgLevel(MSG::DEBUG)
  //m_higgsWeightTool.setTypeAndName("xAOD::HiggsWeightTool/higgsWeightTool");
  //m_higgsWeightTool.setTypeAndName("xAOD::TruthWeightTool");
  //ANA_CHECK(m_higgsWeightTool.initialize());
  m_higgsWeightTool = new xAOD::HiggsWeightTool("higgsWeightTool");
  bool mode_ggZH = false;
  if ( sampleName.Contains("345057") ||  sampleName.Contains("345058") ) {
    mode_ggZH = true;
  }
  float WeightCutOff = -1.0;
  //m_config->getif<bool>("mode_ggZH", mode_ggZH); // we don't use this any more
  m_config->getif<float>("WeightCutOff", WeightCutOff);
  if (mode_ggZH)
    m_higgsWeightTool->setProperty("ForceGGZH", true);
  else 
    m_higgsWeightTool->setProperty("ForceVH", true);
  if ( WeightCutOff > 0. ) m_higgsWeightTool->setProperty("WeightCutOff", WeightCutOff);
  m_higgsWeightTool->initialize();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisBase_STXS :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  xAOD::TEvent* event = wk()->xaodEvent();
  m_PDFweights.clear(); // TODO add the initilization function
  m_QCDweights.clear(); // TODO add the initilization function
  // print every 100 events, so we know where we are:
  if( m_debug || (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));
  // check if the event is data or MC
  //  (many tools are applied either to data or MC)
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    m_isMC = true; // can do something with this later
  }

  //set MC channel number
  m_mcChannel = m_isMC ? eventInfo->mcChannelNumber() : -1;

  if (!m_xSectionProvider) {
    Error("initializeChannel()", "XSection provider not initialized!");
    return EL::StatusCode::FAILURE;
  }

  if(!m_isMC) m_histNameSvc->set_sample("data");
  else m_histNameSvc->set_sample(m_xSectionProvider->getSampleName(m_mcChannel));

  // -> get event weight - fill m_weight
  m_weight = 1.0;
  m_VHEW   = 1.0;
  // --> generator weight
  if (m_isMC) m_weight *= eventInfo->mcEventWeight(0);
  if (m_debug) Info("execute()", "Event weight = %f", m_weight );
  // --> Xsection
  float sigmaEff = m_xSectionProvider->getXSection(m_mcChannel);
  // --> get sum of weights from text file
  if (!m_sumOfWeightsProvider) {
    Error("applyLumiWeight()", "SumOfWeights provider not initialized!");
    return EL::StatusCode::FAILURE;
  }
  // --> we are normalising to MC lumi, sumOfWeights calculated per m_mcChannel
  double sumOfWeights = m_sumOfWeightsProvider->getsumOfWeights(m_mcChannel);
  //m_weight *= sigmaEff / sumOfWeights;
  m_XS = sigmaEff;
  m_SumOfWeight = sumOfWeights;

  // --> scale to desired luminosity
  //m_weight *= (m_luminosity * 1e3); // the MC is scaled to 1pb-1 but m_luminosity is in fb-1
  m_lumi = (m_luminosity * 1e3);

  // TODO: add nominal and variation
  m_histNameSvc -> reset();

  // Save Histogram for Truth Acceptance
  int stage1(0);
  m_EventNumber = eventInfo->eventNumber();
  if ( m_isMC && eventInfo->isAvailable<int>("HTXS_Stage1_Category_pTjet30") ) {
    stage1   = eventInfo->auxdata<int>("HTXS_Stage1_Category_pTjet30");
    if (m_debug) {
      if (m_EventNumber != 1783463) return EL::StatusCode::SUCCESS;
      Info("execute()", "Event counter = %i and stage = %i and Event number = %i", m_eventCounter, stage1, eventInfo->eventNumber() );
    }
  }
  // retrieve HTXS information

  m_HTXS_Higgs_pt = eventInfo->auxdata<float>("HTXS_Higgs_pt");
  m_HTXS_Higgs_eta = eventInfo->auxdata<float>("HTXS_Higgs_eta");
  m_HTXS_Higgs_phi = eventInfo->auxdata<float>("HTXS_Higgs_phi");
  m_HTXS_Higgs_m = eventInfo->auxdata<float>("HTXS_Higgs_m");
  m_HTXS_V_pt = eventInfo->auxdata<float>("HTXS_V_pt");
  m_HTXS_V_eta = eventInfo->auxdata<float>("HTXS_V_eta");
  m_HTXS_V_phi = eventInfo->auxdata<float>("HTXS_V_phi");
  m_HTXS_V_m = eventInfo->auxdata<float>("HTXS_V_m");
  m_HTXS_prodMode = eventInfo->auxdata<int>("HTXS_prodMode");
  m_HTXS_Stage0_Category = eventInfo->auxdata<int>("HTXS_Stage0_Category");
  m_HTXS_Stage1_Category_pTjet25 = eventInfo->auxdata<int>("HTXS_Stage1_Category_pTjet25");
  m_HTXS_Stage1_Category_pTjet30 = eventInfo->auxdata<int>("HTXS_Stage1_Category_pTjet30");
  m_HTXS_Njets_pTjet25 = eventInfo->auxdata<int>("HTXS_Njets_pTjet25");
  m_HTXS_Njets_pTjet30 = eventInfo->auxdata<int>("HTXS_Njets_pTjet30");

  //m_histSvc->BookFillHist("stage1", 1000, 0, 1000, stage1, m_weight);
  //fillStage1Bin(stage1,"_NOVHEW");
  //fillStage1Bin(stage1,"_NOVHEW",false, m_HTXS_V_pt);

  /*
  int fineIndex(0);
  if ( m_isMC && eventInfo->isAvailable<int>("HTXS_Stage1_FineIndex_pTjet30")) {
    fineIndex = eventInfo->auxdata<int>("HTXS_Stage1_FineIndex_pTjet30");
    if (stage1 / 100 == 8) {
      fineIndex = 49 + (stage1%100);
    }
    if ((stage1/100 == 1) && (eventInfo->auxdata<int>("HTXS_prodMode")==5)) {
      fineIndex += 52;  // split off ggZH_gg2H bins
    }
    if (m_debug) Info("execute()", "Event number = %i and fine stage = %i", m_eventCounter, fineIndex );
  }
  */
  //m_histSvc->BookFillHist("fineIndex", 65, - 0.5, 65 - 0.5, fineIndex, m_weight);


  // EW Correction
  // first check if we can retrieve the truth particle in the event
  if (m_doVHNLOEWK) ANA_CHECK(event->retrieve( m_truthParts, "TruthParticles"));
  if (m_debug && m_doVHNLOEWK) Info("execute ()", "Size of the truth particles %i", m_truthParts->size());
  float truthPt = -1.0;
  //ANA_CHECK(RetrieveTruthVPT(truthPt));
  if (m_doVHNLOEWK){
    RetrieveTruthVPT(truthPt);
    applyCS(truthPt);// here we set the m_VHEW, EW correction factor
  }

  //m_weight *= m_VHEW;
  //fillStage1Bin(stage1);
  //fillStage1Bin(stage1,"",false,m_HTXS_V_pt);

  // HiggsWeight
  xAOD::HiggsWeights higgsWeights = m_higgsWeightTool->getHiggsWeights(m_HTXS_Njets_pTjet30,m_HTXS_Higgs_pt,m_HTXS_Stage1_Category_pTjet30);

  m_NominalWeight = higgsWeights.nominal;
  m_Weight0 = higgsWeights.weight0;
  m_PDFWeight0 = higgsWeights.pdf0;
  m_alphaS_up = higgsWeights.alphaS_up;
  m_alphaS_dn = higgsWeights.alphaS_dn;
  int NPDF = higgsWeights.pdf4lhc_unc.size();
  for( int ipdf(0); ipdf < NPDF; ipdf++)  {
    m_PDFweights.push_back( higgsWeights.pdf4lhc_unc[ipdf]);
  }

  int NQCD = higgsWeights.qcd.size();
  for (int iqcd(0); iqcd < NQCD; iqcd++)  {
    m_QCDweights.push_back( higgsWeights.qcd[iqcd] );
  }

  if (m_debug) {
    Info("execute ()", "HiggsWeight weight0   : %.6f", higgsWeights.weight0);
    Info("execute ()", "HiggsWeight   PDF0    : %.6f", higgsWeights.pdf0);
    Info("execute ()", "HiggsWeight nominal   : %.6f", higgsWeights.nominal);
    Info("execute ()", "HiggsWeight alphaS_up : %.6f", higgsWeights.alphaS_up);
    Info("execute ()", "HiggsWeight alphaS_dn : %.6f", higgsWeights.alphaS_dn);
    int NPDF = higgsWeights.pdf4lhc_unc.size();
    for( int ipdf(0); ipdf < NPDF; ipdf++)  {
      Info("execute ()", "HiggsWeight  %2i  PDF  : %.6f", ipdf+1, higgsWeights.pdf4lhc_unc[ipdf]);
    }
    int NQCD = higgsWeights.qcd.size();
    for (int iqcd(0); iqcd < NQCD; iqcd++)  {
      Info("execute ()", "HiggsWeight  %2i  QCD  : %.6f", iqcd+1, m_QCDweights.at(iqcd));
    }
  }

  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisBase_STXS :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisBase_STXS :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  xAOD::TEvent* event = wk()->xaodEvent();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisBase_STXS :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  m_histSvc->Write(wk());
  return EL::StatusCode::SUCCESS;
}



//EL::StatusCode AnalysisBase_STXS :: fillStage1Bin ( int stage1, string remark = "", bool STXS_Stage1_Base = true, float ptV = -1.0 )
EL::StatusCode AnalysisBase_STXS :: fillStage1Bin ( int stage1, string remark, bool STXS_Stage1_Base, float ptV )
{
  string sample_name = m_histNameSvc->get_sample();

  static std::string Stage1Names_Fine[37] = {
  // qq -> ZH NUNU 7
  "QQ2HNUNU_FWDH","QQ2HNUNU_PTV_0_75","QQ2HNUNU_PTV_75_150",
  "QQ2HNUNU_PTV_150_250_0J","QQ2HNUNU_PTV_150_250_GE1J","QQ2HNUNU_PTV_GT250_0J","QQ2HNUNU_PTV_GT250_GE1J",
  // gg -> ZH NUNU 7
  "GG2HNUNU_FWDH","GG2HNUNU_PTV_0_75","GG2HNUNU_PTV_75_150",
  "GG2HNUNU_PTV_150_250_0J","GG2HNUNU_PTV_150_250_GE1J","GG2HNUNU_PTV_GT250_0J","GG2HNUNU_PTV_GT250_GE1J",
  // qq -> WH 7
  "QQ2HLNU_FWDH","QQ2HLNU_PTV_0_75","QQ2HLNU_PTV_75_150",
  "QQ2HLNU_PTV_150_250_0J","QQ2HLNU_PTV_150_250_GE1J","QQ2HLNU_PTV_GT250_0J","QQ2HLNU_PTV_GT250_GE1J",
  // qq -> ZH LL 7
  "QQ2HLL_FWDH","QQ2HLL_PTV_0_75","QQ2HLL_PTV_75_150",
  "QQ2HLL_PTV_150_250_0J","QQ2HLL_PTV_150_250_GE1J","QQ2HLL_PTV_GT250_0J","QQ2HLL_PTV_GT250_GE1J",
  // gg -> ZH LL 7
  "GG2HLL_FWDH","GG2HLL_PTV_0_75","GG2HLL_PTV_75_150",
  "GG2HLL_PTV_150_250_0J","GG2HLL_PTV_150_250_GE1J","GG2HLL_PTV_GT250_0J","GG2HLL_PTV_GT250_GE1J",
  // unknown
  "UNKNOWN",
  // total, always filled
  "TOTAL",
  };

  // stage1 category
  static std::string Stage1Names_Base[25] = {
  // qq -> ZH
  "QQ2HNUNU_FWDH","QQ2HNUNU_PTV_0_150","QQ2HNUNU_PTV_150_250_0J","QQ2HNUNU_PTV_150_250_GE1J","QQ2HNUNU_PTV_GT250",
  // gg -> ZH
  "GG2HNUNU_FWDH","GG2HNUNU_PTV_0_150","GG2HNUNU_PTV_GT150_0J","GG2HNUNU_PTV_GT150_GE1J",
  // qq -> WH
  "QQ2HLNU_FWDH","QQ2HLNU_PTV_0_150","QQ2HLNU_PTV_150_250_0J","QQ2HLNU_PTV_150_250_GE1J","QQ2HLNU_PTV_GT250",
  // qq -> ZH
  "QQ2HLL_FWDH","QQ2HLL_PTV_0_150","QQ2HLL_PTV_150_250_0J","QQ2HLL_PTV_150_250_GE1J","QQ2HLL_PTV_GT250",
  // gg -> ZH
  "GG2HLL_FWDH","GG2HLL_PTV_0_150","GG2HLL_PTV_GT150_0J","GG2HLL_PTV_GT150_GE1J",
  // unknown
  "UNKNOWN",
  // total, always filled
  "TOTAL",
  };

  // std::string stage1_name = reader.TranslateStage1Bin(HTXS_Stage1_Category_pTjet30);
  std::string stage1_name = m_reader->STXS_ParseBin (stage1, m_mcChannel, STXS_Stage1_Base, ptV, m_HTXS_Njets_pTjet30);

  string fillname = sample_name+remark+"_Stage1";

  if (STXS_Stage1_Base) {
    // for a certain bin
    m_histSvc->BookFillCutHist(fillname+"Base", length(Stage1Names_Base), Stage1Names_Base, stage1_name, m_weight);
    m_histSvc->BookFillCutHist(fillname+"Base", length(Stage1Names_Base), Stage1Names_Base, "TOTAL",     m_weight);

    // total
    m_histSvc->BookFillCutHist(fillname+"Base"+"_NoWeight", length(Stage1Names_Base), Stage1Names_Base, stage1_name, 1.);
    m_histSvc->BookFillCutHist(fillname+"Base"+"_NoWeight", length(Stage1Names_Base), Stage1Names_Base, "TOTAL",     1.);
  } else {

    // for a certain bin
    m_histSvc->BookFillCutHist(fillname+"Fine", length(Stage1Names_Fine), Stage1Names_Fine, stage1_name, m_weight);
    m_histSvc->BookFillCutHist(fillname+"Fine", length(Stage1Names_Fine), Stage1Names_Fine, "TOTAL",     m_weight);

    // total
    m_histSvc->BookFillCutHist(fillname+"Fine"+"_NoWeight", length(Stage1Names_Fine), Stage1Names_Fine, stage1_name, 1.);
    m_histSvc->BookFillCutHist(fillname+"Fine"+"_NoWeight", length(Stage1Names_Fine), Stage1Names_Fine, "TOTAL",     1.);

  }

  return EL::StatusCode::SUCCESS;
}


/*
std::string AnalysisBase_STXS :: TranslateStage1Bin (int stage1)
{
   std::string stage1_name = "";
   // qq -> WH
   if(stage1 == 300) stage1_name =  "QQ2HLNU_FWDH";
   else if(stage1 == 301) stage1_name =  "QQ2HLNU_PTV_0_150";
   else if(stage1 == 302) stage1_name =  "QQ2HLNU_PTV_150_250_0J";
   else if(stage1 == 303) stage1_name =  "QQ2HLNU_PTV_150_250_GE1J";
   else if(stage1 == 304) stage1_name =  "QQ2HLNU_PTV_GT250";
   // qq -> ZH
   else if(stage1 == 400) stage1_name =  "QQ2HLL_FWDH";
   else if(stage1 == 401) stage1_name =  "QQ2HLL_PTV_0_150";
   else if(stage1 == 402) stage1_name =  "QQ2HLL_PTV_150_250_0J";
   else if(stage1 == 403) stage1_name =  "QQ2HLL_PTV_150_250_GE1J";
   else if(stage1 == 404) stage1_name =  "QQ2HLL_PTV_GT250";
   // gg -> ZH
   else if(stage1 == 500) stage1_name =  "GG2HLL_FWDH";
   else if(stage1 == 501) stage1_name =  "GG2HLL_PTV_0_150";
   else if(stage1 == 502) stage1_name =  "GG2HLL_PTV_GT150_0J";
   else if(stage1 == 503) stage1_name =  "GG2HLL_PTV_GT150_GE1J";
   else {
     Info("TranslateStage1Bin()", "The stage bin (%i) is unknow, so the name will be set as UNKNOWN", stage1);
     stage1_name =  "UNKNOWN";
   }
   return stage1_name;

}
*/

EL::StatusCode AnalysisBase_STXS::initializeCorrsAndSysts ()
{
  // in princeple, this code is running the 13TeV MC
  //std::string comEnergy = "13TeV";

  // create debug name
  TString csname = "13TeV_ZeroLepton"; // it's always ZeroLepton, but it's a fake name, won't affect EW correction

  Info("initializeCorrsAndSysts()", "Initializing CorrsAndSysts for %s", csname.Data());

  m_corrsAndSysts = new CorrsAndSysts(csname);

  return EL::StatusCode::SUCCESS;

} // initializeCorrsAndSysts

EL::StatusCode AnalysisBase_STXS::applyCS ( float truthPt )
{
  if (!m_doVHNLOEWK) return EL::StatusCode::SUCCESS;
  else if (!m_corrsAndSysts){
    Error("applyCS()", "CorrsAndSysts does not exist!");
    return EL::StatusCode::FAILURE;
  }

  // get the information on the sample
  // before it's processed, we need check if m_xSectionProvider and m_histNameSvc are ready
  if ( (!m_xSectionProvider) || (!m_histNameSvc) ){
    Error("applyCS()", "xSectionProvider or histNameSvc or both are not ready!");
    return EL::StatusCode::FAILURE;
  }

  std::string sampleNameClean       = m_histNameSvc->get_sample();
  std::string detailSampleNameClean = m_xSectionProvider->getSampleDetailName(m_mcChannel);
  if (m_debug) Info("applyCS()", "sample name: %s and detailed one: %s", sampleNameClean.c_str(), detailSampleNameClean.c_str() );
  sampleCS    sample("temp");
  sample.setup_sample(sampleNameClean, detailSampleNameClean);

  TString sampleName                 = sample.get_sampleName();
  TString detailsampleName           = sample.get_detailsampleName();

  CAS::EventType evtType             = m_corrsAndSysts->GetEventType(sampleName);
  CAS::DetailEventType detailevtType = m_corrsAndSysts->GetDetailEventType(detailsampleName);
  double EW_corr = 1.0;
  EW_corr = m_corrsAndSysts->Get_HiggsNLOEWKCorrection( evtType, truthPt );
  if (m_debug) Info("applyCS()", "EW correction factor = %f while the truthPt = %f ", EW_corr, truthPt);
  m_VHEW = EW_corr;

  return EL::StatusCode::SUCCESS;
} // applyCS

EL::StatusCode AnalysisBase_STXS::RetrieveTruthVPT(float &truthPt)
{
  if (!m_histNameSvc){
    Error("RetrieveVPT()", "histNameSvc not ready");
    return EL::StatusCode::FAILURE;
  }
  std::string samplename       =  m_histNameSvc->get_sample();
  if ( samplename == "qqZllH125" || samplename == "qqZvvH125" || 
       samplename == "qqWlvH125" || samplename == "ggZllH125" || 
       samplename == "ggZvvH125")
  {
    int pdgId = 23;
    if(samplename == "qqWlvH125") pdgId = 24;
    if(!m_truthParts){
      // check if truth container exists!
      Error("RetrieveVPT()", "truthParts not ready");
      return EL::StatusCode::FAILURE;
    }else{
      for (auto *part : *m_truthParts)
      {
        if(fabs(part->pdgId())==pdgId && part->status()==62) truthPt = (part->pt());
      }
    }
  } else {
    Error("RetrieveVPT()", "The sample name is not supported right now!");
    return EL::StatusCode::FAILURE;
  }
  return EL::StatusCode::SUCCESS;
}
