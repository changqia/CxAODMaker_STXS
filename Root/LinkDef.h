#include <CxAODMaker_STXS/AnalysisBase_STXS.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class AnalysisBase_STXS+;
#endif
