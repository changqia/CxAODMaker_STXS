#ifndef CxAODMaker_STXS_AnalysisBase_STXS_H
#define CxAODMaker_STXS_AnalysisBase_STXS_H

#include <EventLoop/Algorithm.h>
#include "CxAODTools/ConfigStore.h"
#include "CxAODReader/HistSvc.h"
#include "TSystem.h"
#include <AsgTools/AnaToolHandle.h>

#include "CxAODTools/XSectionProvider.h"
#include "CxAODTools/sumOfWeightsProvider.h"
#include "CxAODReader_VHbb/AnalysisReader_VHQQ.h"

// VH EW
#include "CorrsAndSysts/CorrsAndSysts.h"

// EDM includes:
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODEventInfo/EventInfo.h"

// HiggsWeight
#include <TruthWeightTools/ITruthWeightTool.h>
#include <TruthWeightTools/HiggsWeightTool.h>

class AnalysisBase_STXS : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  int m_eventCounter; //!
  std::string m_outputName;



  // this is a standard constructor
  AnalysisBase_STXS ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  void    setConfig (ConfigStore *config) {
    m_config = config;
  }
  EL::StatusCode fillStage1Bin ( int stage1, string remark = "", bool STXS_Stage1_Base = true, float ptV = -1.0 );
  //std::string    TranslateStage1Bin (int stage1);
  // used for VH EW
  EL::StatusCode initializeCorrsAndSysts ();
  EL::StatusCode applyCS (float truthPt);
  EL::StatusCode RetrieveTruthVPT( float &truthPt );

protected:
  ConfigStore          *m_config;
  HistSvc              *m_histSvc; // !
  HistNameSvc          *m_histNameSvc; // !
  XSectionProvider     *m_xSectionProvider; // !
  sumOfWeightsProvider *m_sumOfWeightsProvider; // !
  AnalysisReader_VHQQ  *m_reader; // !
  double               m_weight; // !
  int                  m_EventNumber; // !
  double               m_lumi; // !
  double               m_XS; // !
  double               m_SumOfWeight; // !
  bool                 m_isMC; // !
  bool                 m_doVHNLOEWK; // !
  int                  m_mcChannel; // !
  float                m_luminosity; // !
  bool                 m_debug;

  // used for VH EW
  CorrsAndSysts        *m_corrsAndSysts; //!
  const xAOD::TruthParticleContainer *m_truthParts; // !

  // used for HiggsWeight
  //asg::AnaToolHandle<xAOD::HiggsWeightTool> m_higgsWeightTool; // !
  xAOD::HiggsWeightTool *m_higgsWeightTool; // !

  // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree
  TTree *m_tree; //!
  //int   m_mcChannel //! also the member but already defined
  //float m_weight; //! also the member but already defined
  float m_VHEW; //! 
  float m_HTXS_Higgs_pt; //!
  float m_HTXS_Higgs_eta; //!
  float m_HTXS_Higgs_phi; //!
  float m_HTXS_Higgs_m; //!
  float m_HTXS_V_pt; //!
  float m_HTXS_V_eta; //!
  float m_HTXS_V_phi; //!
  float m_HTXS_V_m; //!
  int   m_HTXS_prodMode; //!
  int   m_HTXS_Stage0_Category; //!
  int   m_HTXS_Stage1_Category_pTjet25; //!
  int   m_HTXS_Stage1_Category_pTjet30; //!
  int   m_HTXS_Njets_pTjet25; //!
  int   m_HTXS_Njets_pTjet30; //!
  float m_NominalWeight; //!
  float m_Weight0; //!
  float m_PDFWeight0; //!
  float m_alphaS_up; //!
  float m_alphaS_dn; //!
  vector<float> m_PDFweights; //!
  vector<float> m_QCDweights; //!

  // this is needed to distribute the algorithm to the workers
  ClassDef(AnalysisBase_STXS, 1);
};

#endif
