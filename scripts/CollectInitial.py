#!/usr/bin/env python
#from ROOT import *
# get the files for TEST
#/afs/in2p3.fr/home/c/cli/public/STXS_Calculator_TEST/

import ROOT
import numpy as np
ROOT.gStyle.SetPaintTextFormat("1.2f")
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

debug = True

signal_names = [
  'qqZvvH125', 'qqWlvH125', 'qqZllH125', 'ggZllH125', 'ggZvvH125',
]

weight_var   = [
  '', # nominal
  '_PDF0',
  '_PDF1',
  '_PDF2',
  '_PDF3',
  '_PDF4',
  '_PDF5',
  '_PDF6',
  '_PDF7',
  '_PDF8',
  '_PDF9',
  '_PDF10',
  '_PDF11',
  '_PDF12',
  '_PDF13',
  '_PDF14',
  '_PDF15',
  '_PDF16',
  '_PDF17',
  '_PDF18',
  '_PDF19',
  '_PDF20',
  '_PDF21',
  '_PDF22',
  '_PDF23',
  '_PDF24',
  '_PDF25',
  '_PDF26',
  '_PDF27',
  '_PDF28',
  '_PDF29',
  '_QCD0',
  '_QCD1',
  '_QCD2',
  '_QCD3',
  '_QCD4',
  '_QCD5',
  '_QCD6',
  '_QCD7',
  '_alphaSdn',
  '_alphaSup',
]

SplitScheme = 'Fine'

InitialYieldsFile = 'Initial/hist-sample.root'

sigs = []

def clone(hist):
  """ Create real clones that won't go away easily """
  #print type(hist)
  h_cl = hist.Clone()
  if hist.InheritsFrom("TH1"):
    h_cl.SetDirectory(0)
  release(h_cl)
  return h_cl

pointers_in_the_wild = set()
def release(obj):
  """ Tell python that no, we don't want to lose this one when current function returns """
  global pointers_in_the_wild
  pointers_in_the_wild.add(obj)
  ROOT.SetOwnership(obj, False)
  return obj

def SetupTopFrame(hist):
  hist.GetYaxis().SetTitleOffset(1.6)
  hist.SetLabelOffset(0.05)

  hist.GetYaxis().SetTitleSize(0.04)
  hist.GetXaxis().SetTitleSize(0.04)

  hist.GetXaxis().SetLabelSize(0.03)
  hist.GetYaxis().SetLabelSize(0.03)

def SetupTopFrameAdv(hist,xtitle,ytitle,maximum):
  hist_frame = clone(hist)
  SetupTopFrame(hist_frame)
  hist_frame.GetXaxis().SetTitle(xtitle)
  hist_frame.GetYaxis().SetTitle(ytitle)
  hist_frame.SetMaximum(maximum*1.5)
  return hist_frame

can_width=2400
can_height = 1200
c1 = ROOT.TCanvas("Yields","Yields",can_width,can_height)
HistVar={}
rootFile = ROOT.TFile(InitialYieldsFile)
leg = ROOT.TLegend( 0.6, 0.78, 0.9, 0.90 )
leg.SetFillStyle(0)
leg.SetBorderSize(0)
#HIST = rootFile.Get(name)
maximum  = -999
for var in weight_var:
  if debug: print 'collecting: ',var.replace('_','')
  HistSum = None
  for signame in signal_names:
    # find the initial binned histograms
    # ggZllH125_wEW_Stage1
    histoname = signame+var+'_Stage1'+SplitScheme
    if debug: print 'histname: %s'%histoname
    histo_initial = rootFile.Get(histoname)
    if debug: print 'histname: %s with Entries %i'%(histoname, histo_initial.GetEntries())
    if HistSum == None: 
      print 'First for ',var.replace('_','')
      HistSum = clone(histo_initial)
    else : HistSum.Add(histo_initial)
  if debug: print 'Variation %s with Entries %i'%(var.replace('_',''), HistSum.GetEntries())
  if var == '':
    histName = 'Nominal'
  else:
    histName = var.replace('_','')
  HistVar[histName]=HistSum
  if maximum < HistSum.GetMaximum() : maximum = HistSum.GetMaximum()
  leg.AddEntry(HistSum, histName, 'l')

HistFrame=SetupTopFrameAdv(HistVar['Nominal'],'Bins','Yields',maximum)
HistFrame.Draw()
for var in weight_var:
  if var == '':
    continue
  else:
    histName = var.replace('_','')
  HistVar[histName].Draw('same')

c1.Print('Initial_Var.png')
