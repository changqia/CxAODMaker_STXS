export target=ReconsYields.v12

hadd $target.Fine/ReconsYields_0lep.root $target.Fine/submitDir_0lep*/hist-*.root
hadd $target.Fine/ReconsYields_1lep.root $target.Fine/submitDir_1lep*/hist-*.root
hadd $target.Fine/ReconsYields_2lep.root $target.Fine/submitDir_2lep*/hist-*.root

#hadd $target.Base/ReconsYields_0lep.root $target.Base/submitDir_0lep*/hist-*.root
#hadd $target.Base/ReconsYields_1lep.root $target.Base/submitDir_1lep*/hist-*.root
#hadd $target.Base/ReconsYields_2lep.root $target.Base/submitDir_2lep*/hist-*.root
