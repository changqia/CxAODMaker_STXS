#!/usr/bin/env python
#from ROOT import *
# get the files for TEST
#/afs/in2p3.fr/home/c/cli/public/STXS_Calculator_TEST/

import ROOT
import numpy as np
ROOT.gStyle.SetPaintTextFormat("1.2f")
ROOT.gStyle.SetOptStat(0)
#ROOT.gROOT.LoadMacro("macros/AtlasStyle.C")
#ROOT.SetAtlasStyle(0.03)
#ROOT.gStyle.SetPalette(7)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

#palette = np.array([ROOT.kWhite, ROOT.kYellow, ROOT.kOrange-3, ROOT.kOrange+10, ROOT.kRed+1, ROOT.kPink+7, ROOT.kGreen+2], dtype=np.int32) 
#stops = np.array( [ 0.00, 0.34, 0.61, 0.84, 1.00 ], dtype=np.float64 )
#red   = np.array([ 0.00, 0.00, 0.87, 1.00, 0.51 ], dtype=np.float64)
#green = np.array([ 0.00, 0.81, 1.00, 0.20, 0.00 ], dtype=np.float64)
#blue  = np.array([ 0.51, 1.00, 0.12, 0.00, 0.00 ], dtype=np.float64)
#ROOT.TColor.CreateGradientColorTable(5, stops, red, green, blue, 255)
#palette = np.array([ROOT.kWhite, ROOT.kYellow, ROOT.kOrange-3, ROOT.kOrange+10, ROOT.kRed+1, ROOT.kPink+7, ROOT.kGreen+2], dtype=np.int32) 
#ROOT.gStyle.SetPalette(7)

debug = True
NumSize = 1.0
LeftMargin = 0.12
BotMargin  = 0.15
LabelSize_X = 0.03
LabelSize_Y = 0.025
can_width = 2600
can_height = 1200

signal_names = [
#  'qqWlvH125',
#  'qqZvvH125',
  'qqZvvH125', 'qqWlvH125', 'qqZllH125', 'ggZllH125', 'ggZvvH125',
]
SplitScheme = 'Base'
#InitialYieldsFile = '/sps/atlas/c/cli/VHbb_Run2/STXS/downloads/InitialYields.root'
#InitialYieldsFile = 'InitialYields.v08.root'
InitialYieldsFile = 'InitialYields.Stag1PP.root'
#InitialYieldsFile = 'InitialYields.root'
#InitialYieldsFile = 'InitialWeight_WHbb.root'
ReconsYieldsFiles = {
'0lep':'ReconsYields.v10.'+SplitScheme+'/ReconsYields_0lep.root',
'1lep':'ReconsYields.v10.'+SplitScheme+'/ReconsYields_1lep.root',
'2lep':'ReconsYields.v10.'+SplitScheme+'/ReconsYields_2lep.root',

#'2lep':'ReconsYields.v06/ReconsYields_2lep.root',
#'0lep':'ReconsYields.v03/ReconsYields_0lep.root',
#'0lep':'ReconsYields.v04/ReconsYields_0lep.root',
#'1lep':'ReconsYields.v04/ReconsYields_1lep.root',
#'2lep':'ReconsYields.v04/ReconsYields_2lep.root',
#'2lep':'ReconsYields.v04/ReconsYields_2lep.New.root',
#'1lep':'ReconsYields.v02/ReconsYields_1lep.root',
#'2lep':'ReconsYields.v02/ReconsYields_2lep.root',
#'1lep':'ReconsYields.v03/submitDir_1lep_fix/hist-WHlv125J_MINLO.root',
#'0lep':'ReconsYields.v03/submitDir_0lep_fix/hist-ZH125J_MINLO.root',
#'0lep':'ReconsYields.v03/submitDir_0lep_fix/hist-ZH125J_MINLO.root',
}

#name = 'qqZllH125_Stage1'
sigs = []

class Signal(object):
  def __init__(self, H, hs, name):
    self.H = clone(H)
    self.name = name
    self.showname = 'default'
    self.bins = {}
    self.vector = None
    self.LabelX = []
    self.LabelY = []
    self.matrices = {}
    self.InitializeReconHistos(hs)
    self.FillTheBins(hs)

  def InitializeReconHistos(self,hs):
    for key in hs:
      if debug: print key
      h = hs[key]
      #x_axis = h.GetXaxis()
      #y_axis = h.GetYaxis()
      if debug:
        print ' Entries:',h.GetEntries()
        #mymatrix = ROOT.TMatrix(x_axis.GetNbins()+2,y_axis.GetNbins()+2,h.GetArray(),"D")
        #mymatrix.Print()
      matrix = Convert2DHistToMatrix(h)
      if debug:
        #np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
        #np.set_printoptions(precision=3, strip_zeros=False)
        np.set_printoptions(precision=2, suppress=True)
        print (matrix)
      self.matrices[key] = matrix
      if not self.LabelY:
        y_axis=h.GetYaxis()
        Nbins=y_axis.GetNbins()
        for count in range(0, Nbins):
          self.LabelY.append(y_axis.GetBinLabel(count+1))
      if debug: print 'the length of self.LabelY is: ',len(self.LabelY)

  def FillTheBins(self,hs):
    if debug: print ' Integral:',self.H.Integral()
    #xaxis = self.H.GetXaxis()
    xaxis = (hs.values()[0]).GetXaxis()
    Nbins = xaxis.GetNbins()
    bin_vals = []
    if debug:
      print 'NBins: ',Nbins
    for count in range(0, Nbins):
      if debug: print xaxis.GetBinLabel(count+1),self.H.GetBinContent(count+1)
      self.bins[xaxis.GetBinLabel(count+1)]=self.H.GetBinContent(count+1)
      self.LabelX.append(xaxis.GetBinLabel(count+1))
      bin_vals.append(self.H.GetBinContent(count + 1))
    self.vector = np.array(bin_vals)
  
  def CheckTheBins(self):
    if debug:
      print 'Now check the bins'
    for BIN in self.LabelX:
      print self.bins[BIN]

  def GetVector(self):
    return self.vector

  def GetMatrices(self):
    ms = []
    for m_name in self.matrices:
      ms.append(self.matrices[m_name])
    return ms

  def GetLabelX(self):
    return self.LabelX

  def GetLabelY(self):
    return self.LabelY

def clone(hist):
  """ Create real clones that won't go away easily """
  #print type(hist)
  h_cl = hist.Clone()
  if hist.InheritsFrom("TH1"):
    h_cl.SetDirectory(0)
  release(h_cl)
  return h_cl

pointers_in_the_wild = set()
def release(obj):
  """ Tell python that no, we don't want to lose this one when current function returns """
  global pointers_in_the_wild
  pointers_in_the_wild.add(obj)
  ROOT.SetOwnership(obj, False)
  return obj

def Convert2DHistToMatrix(hist):
  x_bins = hist.GetNbinsX()
  y_bins = hist.GetNbinsY()
  bins = np.zeros((x_bins,y_bins))
  for y_bin in xrange(y_bins):
    for x_bin in xrange(x_bins):
      bins[x_bin,y_bin] = hist.GetBinContent(x_bin + 1,y_bin + 1)
  return bins

def ConvertMatrixTo2DHist(matrix,name):
  nbinsx,nbinsy = matrix.shape
  hist = ROOT.TH2D(name, name, nbinsx, -0.5, nbinsx-0.5, nbinsy, -0.5, nbinsy-0.5);
  for y_bin in xrange(nbinsy):
    for x_bin in xrange(nbinsx):
      hist.SetBinContent(x_bin + 1,y_bin + 1, matrix[x_bin,y_bin])
  return hist

def Set2DHistLabels(hist,labelx,labely):
  nbinsx = hist.GetNbinsX()
  nbinsy = hist.GetNbinsY()
  for x_bin in xrange(nbinsx):
    hist.GetXaxis().SetBinLabel(x_bin+1,labelx[x_bin])
  for y_bin in xrange(nbinsy):
    hist.GetYaxis().SetBinLabel(y_bin+1,labely[y_bin])
  hist.GetXaxis().SetLabelSize(LabelSize_X)
  hist.GetYaxis().SetLabelSize(LabelSize_Y)


rootFile = ROOT.TFile(InitialYieldsFile)
#HIST = rootFile.Get(name)
for signame in signal_names:

  # find the initial binned histograms
  # ggZllH125_wEW_Stage1
  histoname = signame+'_Stage1'+SplitScheme
  histo_initial = rootFile.Get(histoname)

  # find the Recons histograms
  #histoname += '_RecoCategory'
  #histoname += 'Bin_RecoCategory_NoWeight_1lep'
  histoname = signame+'_Stage1Bin_RecoMapEPS'+SplitScheme
  if debug: print ' name of the recon. histo.:',histoname
  histo_reco = {}
  for name in ReconsYieldsFiles: 
    if debug: print ' name of the recon. file:',name
    recon_file = ROOT.TFile(ReconsYieldsFiles[name])
    h = recon_file.Get(histoname)
    if h :
      histo_reco[name]= clone(h)
      if debug: print ' Entries:',h.GetEntries()
    else :
      print '%s doesnt exist in the file %s'%(histoname,ReconsYieldsFiles[name])
      continue

  if len(histo_reco) == 0:
    if debug : print 'skip %s due to no histo found in any ReconsYieldsFiles'%signame
    continue
  sig = Signal(histo_initial, histo_reco, signame)
  sig.CheckTheBins()
  sigs.append(sig)

vectors = []
matrics = []
for sig in sigs:
  vectors.append(sig.GetVector())
  for m in sig.GetMatrices():
    if debug: print m.shape
    matrics.append(m)

yields = sum(matrics)
if debug: 
  print (yields)

# Yields
hist = clone(ConvertMatrixTo2DHist(yields,'Yields'))

if debug:
  print 'The X-label',sigs[0].GetLabelX()
  print 'The Y-label',sigs[0].GetLabelY()
Set2DHistLabels(hist,sigs[0].GetLabelX(),sigs[0].GetLabelY())
c1 = ROOT.TCanvas("Yields","Yields",can_width,can_height)
c1.SetBottomMargin(BotMargin)
c1.SetLeftMargin(LeftMargin)
hist.SetMarkerSize(NumSize)
#hist.SetTextAngle(90.)
hist.Draw("COLZTEXT45")
c1.Print('Yields.png')
#c1.Delete()
#hist.Delete()


# purity
categories = yields.sum(axis=0)
if debug:
  print categories
  print 'shape of the categories: ',categories.shape
purity = yields/(categories*0.5)*100 # double couting due to the TOTL colum
hist = clone(ConvertMatrixTo2DHist(purity,'Purity'))
Set2DHistLabels(hist,sigs[0].GetLabelX(),sigs[0].GetLabelY())
c1 = ROOT.TCanvas("Purity","Purity",can_width,can_height)
c1.SetBottomMargin(BotMargin)
c1.SetLeftMargin(LeftMargin)
hist.SetMarkerSize(NumSize)
hist.Draw("COLZTEXT45")
c1.Print('Purity.png')
#c1.Delete()
#hist.Delete()

# acceptance
print sum(vectors)

if debug:
  print 'shape of the yields: ',yields.shape
  print 'shape of the vector: ',sum(vectors).shape
acceptance = (yields.T/sum(vectors)).T*100
hist = clone(ConvertMatrixTo2DHist(acceptance,'Acceptance'))
Set2DHistLabels(hist,sigs[0].GetLabelX(),sigs[0].GetLabelY())
c1 = ROOT.TCanvas("Acceptance","Acceptance",can_width,can_height)
c1.SetBottomMargin(BotMargin)
c1.SetLeftMargin(LeftMargin)
hist.SetMarkerSize(NumSize)
hist.Draw("COLZTEXT45")
c1.Print('Acceptance.png')
#c1.Delete()
#hist.Delete()

